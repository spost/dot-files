[colors]
background = #ee222222
background-alt = #444
foreground = #dddfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[section/base]
width = 100%
height = 55
radius = 0
fixed-center = true
bottom = true
background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
line-color = #f00

padding-left = 0
padding-right = 3

module-margin-left = 3
module-margin-right = 3

font-0 = OpenSans:size=20;1

cursor-click = pointer
cursor-scroll = ns-resize

[bar/laptop]
inherit = section/base
modules-left = bspwm
modules-center = xwindow
modules-right = pulseaudio battery load date

[bar/work]
inherit = section/base
modules-left = bspwm
modules-center = xwindow
modules-right = weather pulseaudio load date

[bar/home]
inherit = section/base
modules-left = bspwm
modules-center = xwindow
modules-right = weather vpnstatus pulseaudio load date
font-0 = OpenSans:size=11;1

[module/xwindow]
type = internal/xwindow
label = %title:0:100:...%

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

[module/date]
type = internal/date
interval = 5

date =
date-alt = "DATE %B %d, %Y   "

time = TIME %I:%M
time-alt = TIME %I:%M:%S

label = %date%%time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume>
label-volume = VOL %percentage%%

label-muted = muted

[module/vpnstatus]
type = custom/script
exec = "curl --silent https://am.i.mullvad.net/json | jq --raw-output 'if .mullvad_exit_ip then "VPN ON" else "VPN OFF" end'"
interval = 60

[module/weather]
type = custom/script
exec = "echo "TEMP $(curl --silent --location 'https://forecast.weather.gov/zipcity.php?inputstring=80501' | htmlq --text '.myforecast-current-lrg')""
interval = 600

[module/battery]
type = custom/script
exec = "echo BATT $(acpi -b | rg --only-matching '\d{1,3}%, \d{0,2}:\d{0,2}')"
interval = 60

[module/load]
type = custom/script
exec = "echo LOAD $(cut --fields='1 2 3' --delimiter ' ' < /proc/loadavg)"
interval = 60


[settings]
screenchange-reload = true

; vim:ft=dosini
