#!/bin/env bash

grim -g "$(hyprctl clients -j | jq  --raw-output --arg active "$(hyprctl monitors -j | jq --raw-output .[0].activeWorkspace.id)" 'map(select(.workspace.id | tostring == $active) | "\(.at[0]),\(.at[1]) \(.size[0])x\(.size[1])")[]' | slurp -r)" - | wl-copy --type image/png
