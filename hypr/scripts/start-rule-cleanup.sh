#! /bin/env bash
sleep 2m
hyprctl keyword windowrule "workspace unset,Steam"
hyprctl keyword windowrule "workspace unset,discord"
hyprctl keyword windowrule "workspace unset,org.telegram.desktop"
hyprctl keyword windowrule "workspace unset,firefox"
