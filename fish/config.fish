# Set normal env vars
set --export --global EDITOR helix
set --export --global VISUAL helix
set --export --global PAGER less
set --export --global NODE_OPTIONS "--max-old-space-size=20000"

set --export GPG_TTY (tty)

# Set colors
set --global fish_color_command green
set --global fish_color_error red
set --global fish_color_end cyan
set --global fish_color_escape yellow
set --global fish_color_param cyan
set --global fish_color_end magenta
set --global fish_color_quote yellow
set --global fish_color_redirection yellow

set --export --global SSH_AUTH_SOCK /run/user/(id -u)/gcr/ssh

abbr --add --global gst 'git status'
abbr --add --global ip 'ip -color'

# Add extra stuff to the path
fish_add_path \
    $HOME/.local/bin \
    $HOME/.cargo/bin \
    $HOME/.nimble/bin \
    $HOME/.dotnet/tools \
    $HOME/.config/yarn/global/node_modules/.bin \
    $HOME/.local/google-cloud-sdk/bin

# Disable greeting
set fish_greeting

# Setup base16-shell
if status --is-interactive && test -f "$HOME/.config/base16-shell/profile_helper.fish"
    set BASE16_SHELL "$HOME/.config/base16-shell/"
    source "$BASE16_SHELL/profile_helper.fish"
end

# use Starship prompt
if type -q starship
    starship init fish | source
end

# use zoxide
if type -q zoxide
    zoxide init fish | source
end

if type -q eza
    alias ls eza
    alias ll 'eza -lgh'
    alias la 'eza -a'
    alias lt 'eza --tree'
    alias lla 'eza -lga'
end
