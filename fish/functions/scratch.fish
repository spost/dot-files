function scratch --description 'Create or edit a scratchpad doc in $SCRATCH_HOME (default ~/scratch)' --argument SUBJECT
    if test -z "$SCRATCH_HOME"
        set --function SCRATCH_HOME "$HOME/scratch"
    end
    mkdir --parents "$SCRATCH_HOME"
    if test -z "$SUBJECT"
        set --function SUBJECT notes
    end
    set DATE (date --iso-8601=date)
    set NOTEFILE "$DATE-$SUBJECT.md"
    $EDITOR "$SCRATCH_HOME/$NOTEFILE"
end
