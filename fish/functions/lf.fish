function lf --description "Run some file manager if I have one"
    if type -q joshuto
        joshuto $argv
    else if type -q lf
        lf $argv
    else if type -q ranger
        ranger $argv
    else
        ls -la
    end
end
