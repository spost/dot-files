function cd
    if test -z "$argv"
        set cdto ~
    else
        set cdto $argv
    end

    builtin cd "$cdto"; and timeout .1 ls --color=always

    if test -f "./.nvmrc" -o -f "./.node-version" && type -q nvm
        nvm install
    end
end
