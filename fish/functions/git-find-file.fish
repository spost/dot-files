function git-find-file
    for branch in $(git rev-list --all)
        git ls-tree -r --name-only $branch | sed 's/^/'$branch': /'
    end | fzf
end
