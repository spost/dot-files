function e --wraps=EDITOR --description 'alias e=$EDITOR'
    $EDITOR $argv
end
