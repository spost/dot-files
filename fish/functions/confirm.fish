set usage "confirm <prompt> <default: y/n>"
function confirm --argument-names prompt default --description "prompt for yes/no input: $usage"
    if test -z "$prompt"
        or not string match --regex --ignore-case --quiet 'y|n' "$default"
        echo $usage
        return 5
    end

    set --function default (string lower $default)

    if test (string lower $default) = y
        set --function full_prompt "$prompt [Y/n] "
    else
        set --function full_prompt "$prompt [y/N] "
    end

    while true
        set --function response (string lower (read -P $full_prompt))

        if test -z "$response"
            set --function response $default
        end

        if test "$response" = y
            return 0
        else if test "$response" = n
            return 1
        else
            echo "Y/N please (got $response)"
        end
    end
end
