#!/usr/bin/env bash

FILE_PATH="$1"
PREVIEW_X_COORD="$2"
PREVIEW_Y_COORD="$3"
PREVIEW_WIDTH="$4"
PREVIEW_HEIGHT="$5"

mkdir -p "$HOME/.cache/joshuto"
TMP_FILE="$HOME/.cache/joshuto/thumbcache.png"

mimetype=$(file --mime-type -Lb "$FILE_PATH")
echo "on_preview_shown called with $FILE_PATH, mimetype is $mimetype" >>$HOME/.cache/joshuto/test

function kclear {
	kitty +kitten icat \
		--transfer-mode=file \
		--clear 2>/dev/null
}

function icat {
	kitty +kitten icat \
		--transfer-mode=file \
		--place "${PREVIEW_WIDTH}x${PREVIEW_HEIGHT}@${PREVIEW_X_COORD}x${PREVIEW_Y_COORD}" \
		"$1" 2>/dev/null
}

case "$mimetype" in
image/*)
	echo "in icat" >> $HOME/.cache/joshuto/test
	kclear
	icat "${FILE_PATH}"
	;;
video/*)
	echo "in video" >> $HOME/.cache/joshuto/test
	kclear
	ffmpegthumbnailer -i "$FILE_PATH" -f -m -c png -s 0 -o - 2>/dev/null |
		kitty +kitten icat --transfer-mode=stream --place \
			"${PREVIEW_WIDTH}x${PREVIEW_HEIGHT}@${PREVIEW_X_COORD}x${PREVIEW_Y_COORD}"
	;;
*)
	kclear
	;;
esac
