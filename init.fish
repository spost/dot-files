#/usr/bin/env fish

set BASE16_HOME $HOME/.config/base16-shell

git clone https://github.com/chriskempson/base16-shell.git $BASE16_HOME
nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'

cp xinitrc $HOME/.xinitrc
