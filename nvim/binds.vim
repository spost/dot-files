" use Space as Leader
let mapleader="\<Space>"

" Save current buffer
nnoremap <Leader>w <cmd>w<CR>

" Exit terminal with <Leader>ESC
tnoremap <Leader><Esc> <C-\><C-n>

" Enter after searching to clear highlights
nnoremap <silent> <CR> <cmd>noh<CR><CR>

" Buffer nav/close
nmap <Tab> <cmd>bnext<CR>
nmap <S-Tab> <cmd>bprevious<CR>
nmap <Leader>x <cmd>bdelete<CR>
nmap <Leader>q <cmd>bdelete<CR>

" File Sidebar
nmap <silent> <Leader>r <cmd>NvimTreeFindFile<CR>
nmap <silent> <Leader>t <cmd>NvimTreeToggle<CR>

" LSP stuff
nnoremap <silent> gr <cmd>lua require('telescope.builtin').lsp_references()<CR>
nnoremap <silent> gd <cmd>lua require('telescope.builtin').lsp_definitions()<CR>
nnoremap <silent> gD <cmd>lua require('telescope.builtin').lsp_implementations()<CR>

nnoremap <silent> <C-n> <cmd>lua vim.diagnostic.goto_next()<CR>
nnoremap <silent> <C-e> <cmd>lua vim.diagnostic.goto_prev()<CR>

nnoremap <silent> lh <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> la <cmd>lua vim.lsp.buf.code_action()<CR>
nnoremap <silent> lr <cmd>lua vim.lsp.buf.rename()<CR>

" File search
nnoremap <C-p> <cmd>lua require('telescope.builtin').find_files()<CR>

" grep
nnoremap <C-f> <cmd>lua require('telescope.builtin').live_grep()<CR>

" workman binds
noremap ) n
noremap ( N
noremap Y y
noremap N J
noremap <Leader>y <C-W><C-H>
noremap <Leader>n <C-W><C-J>
noremap <Leader>e <C-W><C-K>
noremap <Leader>o <C-W><C-L>
map y <Left>
map n <Down>
map e <Up>
map o <Right>
