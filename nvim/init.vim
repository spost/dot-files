filetype plugin indent on
syntax enable

" disable providers we don't want or use
let g:loaded_python3_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_node_provider = 0
let g:loaded_perl_provider = 0


if has('termguicolors')
	set termguicolors
endif

lua require("plugins")
source ~/.config/nvim/binds.vim

""" Settings
" splits should make sense
set splitbelow
set splitright

" so should searching
set ignorecase
set smartcase

" obviously we want line numbers
set number

" indentation shouldn't be stupid either
set shiftround
set smarttab
set smartindent

" setup for folding, but don't by default
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set nofoldenable

" allow using a lot of memory for pattern matching
set maxmempattern=2000000

" persist undo
set undodir=expand('~/.local/share/nvim/undo')

" redundant with statusline
set noshowmode
set nomodeline

" arguably nicer tab management
set hidden
set showtabline=2

" show hidden characters in a nice way
set list
set listchars=tab:\ \ ,trail:·,extends:→,precedes:←

" readable word wrapping
set breakindent
set linebreak

" This is all crap we don't want in Vim
set wildignore+=*.class/*,.class
set wildignore+=*.git/*,.git
set wildignore+=*.yarn/*,.yarn
set wildignore+=*/tmp/*,*.so,*.swp,*.zip " macOS/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe " Windows
set wildignore+=*build/*,build
set wildignore+=*coverage/*,coverage
set wildignore+=*node_modules/*,node_modules

set guifont=monospace:h10
if exists("g:neovide")
	let g:neovide_cursor_animation_length = 0.08
	let g:neovide_cursor_trail_length = 0.02
endif

" mouse is useful
set mouse=a
set mousemodel=extend

" 8-wide tabs are silly
set tabstop=2 softtabstop=2 shiftwidth=2

fu! SetBang(v) range
	if a:v == 1
		normal gv
	endif
	let l:t = &shellredir
	let &shellredir = ">%s\ 2>/dev/tty"
	let @" = join(systemlist(input("\"!"))," ")
	let &shellredir = l:t
endf
nnoremap "! :cal SetBang(0)<cr>
xnoremap "! :cal SetBang(1)<cr>
