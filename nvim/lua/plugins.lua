-- this file can be loaded by calling `lua require("plugins")` from your init.vim
-- set up if not installed
local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
	packer_bootstrap = vim.fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
end

vim.cmd([[packadd packer.nvim]])
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require("packer").startup(function()
	use("wbthomason/packer.nvim")

	-- Lightweight and pretty statusline
	use("ojroques/nvim-hardline")

	-- Nice searching
	use("ggandor/leap.nvim")

	-- Pretty Colors
	use("ellisonleao/gruvbox.nvim")

	-- guess indentation
	use("nmac427/guess-indent.nvim")

	-- Allow repeating complex commands that . can't normally
	use("tpope/vim-repeat")

	-- ys/cs/ds for modifying things surrounding objects
	use("tpope/vim-surround")

	-- Commenting with treesitter support
	use("echasnovski/mini.comment")

	-- Binding asssist
	use("folke/which-key.nvim")

	-- Native Language Server Protocol client config
	use("neovim/nvim-lspconfig")
	-- Completion for same
	use({
		"hrsh7th/nvim-cmp",
		requires = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
			{ "hrsh7th/cmp-vsnip", requires = { "hrsh7th/vim-vsnip", "rafamadriz/friendly-snippets" } },
		},
	})
	-- Function signature help for same
	use("ray-x/lsp_signature.nvim")

	-- Parser-based highlighting
	use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })

	-- Formatter
	use("mhartington/formatter.nvim")

	-- File sidebar
	use({
		"kyazdani42/nvim-tree.lua",
		requires = { "kyazdani42/nvim-web-devicons" },
	})

	-- Nice Ctrl-P
	use({
		"nvim-telescope/telescope.nvim",
		branch = "0.1.x",
		requires = {
			"nvim-lua/popup.nvim",
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope-ui-select.nvim",
			{ "nvim-telescope/telescope-fzf-native.nvim", run = "make" },
		},
	})

	-- special syntaxes
	use("towolf/vim-helm")
	use("edgedb/edgedb-vim")
	use("elkowar/yuck.vim")

	if packer_bootstrap then
		require("packer").sync()
	end
end)
