require("lsp_signature").setup({
	bind = true,
	handler_opts = { border = "none" },
	hint_enable = true,
	hint_prefix = "⤦ ",
	floating_window = false,
	doc_lines = 0,
})
