local leap = require("leap")
leap.set_default_keymaps()
leap.opts.case_sensitive = false
leap.opts.safe_labels = {}
leap.opts.labels = {
	"t",
	"n",
	"h",
	"e",
	"s",
	"o",
	"a",
	"i",
	"d",
	"p",
	"u",
	"r",
	"d",
	"f",
	"w",
	"T",
	"N",
	"H",
	"E",
	"S",
	"O",
	"A",
	"I",
	"D",
	"P",
	"U",
	"R",
	"D",
	"F",
	"W",
}
vim.api.nvim_set_hl(0, "LeapBackdrop", { link = "Comment" })

-- Bidirectional search in the current window is just a specific case of the
-- multi-window mode - set `target_windows` to a table containing the current
-- window as the only element:
function leap_bidirectional()
	leap.leap({ target_windows = { vim.api.nvim_get_current_win() } })
end
