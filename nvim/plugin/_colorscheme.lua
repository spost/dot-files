require("gruvbox").setup({
	italic = {
		strings = false,
		comments = true,
		operators = false,
		folds = true,
	},
	contrast = "hard", -- can be "hard", "soft" or empty string
})
vim.o.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme gruvbox]])
