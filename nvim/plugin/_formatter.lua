require("formatter").setup({
	filetype = {
		typescriptreact = { require("formatter.filetypes.typescript").prettier },
		typescript = { require("formatter.filetypes.typescript").prettier },
		json = { require("formatter.filetypes.json").prettier },
		markdown = { require("formatter.filetypes.markdown").prettier },
		lua = { require("formatter.filetypes.lua").stylua },
		fish = { require("formatter.filetypes.fish").fishindent },
		rust = { require("formatter.filetypes.rust").rustfmt },
	},
})

vim.cmd([[
	augroup FormatAutogroup
		autocmd!
		autocmd BufWritePost * FormatWrite
	augroup END
]])
