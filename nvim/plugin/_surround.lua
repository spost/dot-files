vim.g.surround_no_mappings = 1

-- generally just "don't conflict with leap lol"
vim.keymap.set("n", "gsd", "<Plug>Dsurround")
vim.keymap.set("n", "gsc", "<Plug>Csurround")
vim.keymap.set("n", "gs", "<Plug>Ysurround")
vim.keymap.set("x", "gs", "<Plug>VSurround")
