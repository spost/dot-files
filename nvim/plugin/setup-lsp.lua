function merge(...)
	local tables_to_merge = { ... }
	assert(#tables_to_merge > 1, "There should be at least two tables to merge them")

	for k, t in ipairs(tables_to_merge) do
		assert(type(t) == "table", string.format("Expected a table as function parameter %d", k))
	end

	local result = tables_to_merge[1]

	for i = 2, #tables_to_merge do
		local from = tables_to_merge[i]
		for k, v in pairs(from) do
			if type(k) == "number" then
				table.insert(result, v)
			elseif type(k) == "string" then
				if type(v) == "table" then
					result[k] = result[k] or {}
					result[k] = merge(result[k], v)
				else
					result[k] = v
				end
			end
		end
	end

	return result
end

local jsonls_cap = vim.lsp.protocol.make_client_capabilities()
jsonls_cap.textDocument.completion.completionItem.snippetSupport = true

local lang_servers = {
	tsserver = { init_options = { maxTsServerMemory = 20000 } },
	gopls = {},
	nimls = {},
	jdtls = {},
	rust_analyzer = {},
	pyright = {},
	eslint = {},
	metals = { cmd = { "metals-vim" } },
	jsonls = { capabilities = jsonls_cap },
}

local lspconfig = require("lspconfig")
local completion_cap = require("cmp_nvim_lsp").default_capabilities()
local no_semantic_tokens = { semanticTokensProvider = nil }

local semantic_tokens = true

for server, config in pairs(lang_servers) do
	if semantic_tokens then
		lspconfig[server].setup(merge(config, { capabilities = completion_cap, autostart = true }))
	else
		lspconfig[server].setup(
			merge(config, { capabilities = merge(completion_cap, no_semantic_tokens), autostart = true })
		)
	end
end
