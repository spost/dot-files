function vpn --argument ACTION
    if which mullvad &> /dev/null
        set STATUS (mullvad status)
        switch $STATUS
            case "Connected*"
                set --function TEXT "VPN ON"
            case "*"
                set --function TEXT "VPN OFF"
        end
        switch $ACTION
            case "click"
                switch $STATUS
                    case "Connected*"
                        mullvad disconnect
                    case "*"
                        mullvad connect
                end
            case "status"
                echo "{\"text\": \"$TEXT\", \"tooltip\": \"$STATUS\"}"
                
        end
    else
        echo "{\"text\": \"NO VPN\"}"
    end 
end
vpn $argv
